var SerialPort = require("serialport");
var path = require("path");
var config = require( path.join(__dirname,'../config'));

var incom = new Buffer([]);

var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database( config.get("database").path );

module.exports = function(http) {
	var io = require('socket.io')(http);

	var port = new SerialPort(config.get("serialport").name, { //COM7
	  "baudRate": config.get('serialport').baudRate
	});

	  port.on('open', function() {
		console.log('Port is open');
		state = "Подключен";
	  });

	  port.on('error', function(err) {
		state = "Отключен";
		console.log('Error: ', err.message);
	  })

	  db.serialize(function() {
		var stmt = db.prepare("INSERT INTO number(value, date) VALUES (?, ?)");

		port.on('data', function (data) {
		  incom = Buffer.concat([incom, data]);
		  while (incom.length >=8) {
			//console.log(incom);
			if (incom.indexOf(0x02) < 0) {
			  incom = new Buffer([]);
			  return;
			}
			if (incom[0] != 0x02) {
			  incom = incom.slice(incom.indexOf(0x02),incom.length);
			  continue;
			}
			if (incom[7] != 0x03) {
			  incom = incom.slice(1, incom.length);
			  continue;
			}
			parsePack(incom.slice(0, 8), function (val) {
		  state = "Работает";
		  stmt.run(val, Date.now() );
		});
		incom = incom.slice(8, incom.length);
		}
	  });
	});

	function parsePack(buf, callback) {
	  var crc = buf[6];
	  var calc_crc = 0;
	  buf[6] = 0;
	  for (var i=0; i<buf.length; i++) {
		calc_crc ^= buf[i];
	  }
	  if (crc == calc_crc) {
		var val = buf.readFloatLE(2);
		//console.log("PACKAGE CORRECT, value:", val);
		io.emit('data', { val: val });
		callback(val);
	  } 
	  //else {
		//console.log("CRC INCORRECT!",buf, "crc:", crc, "calc_crc:", calc_crc);
	  //}
	}

	io.on('connection', function(socket){
		console.log('a user connected');
		//io.emit('data', { val:  });
	});
}