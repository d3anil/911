var express = require('express');
var bodyParser = require('body-parser')


var path = require('path');

var config = require( path.join(__dirname,'config'));
var log = require(path.join(__dirname,'libs/log'))(module);
var HttpError = require(path.join(__dirname,'error')).HttpError;

var app = express();
var http = require('http').Server(app);

var state = "none";

app.set('port', config.get('port'));
app.engine('ejs', require('ejs-locals'))
app.set('views', path.join(__dirname, '/templates'));
app.set('view engine', 'ejs');

app.use(express.favicon());
if (app.get('env') == 'development') {
    app.use(express.logger('dev'));
} else {
    app.use(express.logger('default'));
}

app.use(express.json());

app.use(bodyParser.json({limit: '5mb'}));
app.use(bodyParser.urlencoded({limit: '5mb', extended: true}));

app.use(require(path.join(__dirname,"middleware/sendHttpError")));

app.use(app.router);

require(path.join(__dirname,'routes'))(app);
require(path.join(__dirname,'database'))(http);


app.use(express.static(path.join(__dirname, 'public')));

//error
app.use(function(err, req, res, next) {
  if (typeof err == 'number') {
    err = new HttpError(err);
  }

  if (err instanceof HttpError) {
    res.sendHttpError(err);
  } else {
    if (app.get('env') == 'development') {
      express.errorHandler()(err, req, res, next);
    } else {
      log.error(err);
      err = new HttpError(500);
      res.sendHttpError(err);
    }
  }
});

http.listen(config.get('port'), function(){
  log.info('Express server listen on port ' + config.get('port'));

});
