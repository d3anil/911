#define PIN A5

void setup() {
  pinMode(PIN, INPUT);
  Serial.begin(9600);
}

void loop() {
  Serial.println( digitalRead(PIN));
}
