// ===== Serial ===== //

#define BAUDRATE 9600
#define STX 0x02
#define ETX 0x03

uint8_t CNT = 0;
// ===== Serial ===== //
// bisiness_logic //
#define PIN A1
double IA, VA;
 
void bisiness_logic_setup();
void bisiness_logic_loop();
// bisiness_logic //
 
void transmit(double value) { 
Serial.write(STX); 
Serial.write(CNT); 

// transmit value // 
byte* data = (byte*) &value; 
Serial.write(data, sizeof(double));
// transmit value // 

// crc // (simple xor) 
uint8_t crc = CNT ^ STX ^ ETX; 
for (int i=0; i<sizeof(double); i++) { 
  crc ^= data[i];
} 
// crc // 

Serial.write(crc); 
Serial.write(ETX); 
CNT++; 
}
 
void setup() {
  Serial.begin(BAUDRATE);
   
  bisiness_logic_setup();
}
 
void loop() {
  bisiness_logic_loop();
}
 
// SETUP //
void bisiness_logic_setup() {
  analogReference(DEFAULT);
  pinMode(A1, INPUT_PULLUP); // ПОДТЯГИВАЮЩИЙ РЕЗИСТОР
}
// SETUP //
 
// LOOP //
void bisiness_logic_loop(){
  double max2 = 0;
  VA = abs( analogRead(PIN) * 5. / 1023. - 4.8852 );
  for (int j=0; j<20; j++) {
    double max1 = IA;
    for (int i=0; i<300; i++) {
      IA = abs( VA*1.58 / 0.775/-16);
      max1 = (max1+IA)/2;
    }
    max2 = (max2 + max1);
  }
  max2 = max2/20;
  max2 = (max2 - int(max2))*1000000 ;

  transmit( max2 );
  //Serial.println();
  //Serial.println(max2);
  delay (10);
}
// LOOP //
