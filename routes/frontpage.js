var path = require('path');
var config = require( path.join(__dirname,'../config'));

exports.get = function(req, res) {
  var data = {
    state: state,
    role:       config.get('role'),
    port:       config.get('port'),
    serialport: config.get("serialport").name,
    baudrate:   config.get("serialport").baudRate,
    hh:         config.get('params').shutdown,
    work:       config.get('params').work
  }
  res.render('frontpage', data);
}

exports.post = function(req, res) {
  console.log(req.body);
  config.set('role', req.body.role);
  config.set('port', req.body.port);
  config.set("serialport:name", req.body.serialport);
  config.set("serialport:baudRate", req.body.baudrate | 0);
  config.set("params:shutdown",  req.body.hh);
  config.set("params:work",  req.body.work);
  config.save(function (err) {
    if (err) throw err;
  });
  res.send({});
}
