var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('./database/sensor.db');

exports.get = function(req, res) {
  var data = [];

  db.serialize(function() {
    db.each("SELECT rowid AS id, value, date FROM number", function(err, row) {
        if (err) throw(err);
        data.push(row);
    }, function(err) {
      if (err) throw(err);
      res.render('alldata', {"data": data});
    });
  });
}
