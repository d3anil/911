module.exports = function(app) {
  app.get ('/', require('./frontpage').get);
  app.post('/', require('./frontpage').post);
  app.get('/all',     require('./alldata').get);
  app.get('/between', require('./between').get);
  app.get('/between1', require('./between1').get);
  app.get('/between2', require('./between2').get);
  app.get('/debug', require('./debugging').get);
  app.get('/tools', require('./tools').get);
  app.post('/tools', require('./tools').post);

  app.get('/select/:date_from/:date_to', require('./select').get);
};
