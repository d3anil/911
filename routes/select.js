var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('./database/sensor.db');

exports.get = function(req, res) {
  var date_from = req.params.date_from;
  var date_to = req.params.date_to;
  var data = [];

  db.serialize(function() {
    db.each("SELECT rowid AS id, value, date FROM number WHERE date BETWEEN " + date_from + " AND " + date_to, function(err, row) {
        if (err) throw(err);
        row.value = Number(row.value);
        data.push(row);
    }, function(err) {
      if (err) throw(err);
      res.send(data);
    });
  });
}
