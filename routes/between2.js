var path = require('path');
var config = require( path.join(__dirname,'../config'));

exports.get = function(req, res) {
  var data = {
    role:       config.get('role')
  }
  res.render('between2', {"data": data, "params":config.get("params")});
}

exports.post = function(req, res) {
  console.log(req.body);
  config.set('role', req.body.role);
  config.save(function (err) {
    if (err) throw err;
  });
  res.send({});
}
