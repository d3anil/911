function render_graph2(data, params, from, to) {
  from = from/60000.0;
  to   = to/60000.0;

  var margin = {top: 0, right: 10, bottom: 64, left: 10},
      width = params.g_width - margin.left - margin.right,
      height = params.g_height - margin.top - margin.bottom,
      barWidth = width/(to - from)/12;

  var x = d3.scale.linear()
      .range([0, width]);

  var y = d3.scale.linear()
      .range([height, 0]);

  var xAxis = d3.svg.axis()
      .scale(x)
      .orient("bottom")
      .ticks(20)
      .tickFormat(function(d) {return new Date(d*60000).toLocaleTimeString();});

  var svg = d3.select("#div_graph2").append("svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
    .append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

  x.domain([from, to]);

  svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis)
    .selectAll("text")
      .attr("dy", -6)
      .attr("dx", -35)
      .attr("transform", "rotate(-90)");

  svg.append("g")
      .append("rect")
      .attr("class", "bar-none")
      .attr("x", 0)
      .attr("width", width)
      .attr("y", 0)
      .attr("height", height)

  svg.selectAll(".bar")
      .data(data)
    .enter().append("rect")
      .attr("class", "bar")
      .attr("x", function(d) {return x(d.date/60000);})
      .attr("width", barWidth)
      .attr("y", 0)
      .attr("height", height)

  svg.selectAll(".bar").attr('fill', function(d, i) {
    if (d.value>params.shutdown)
      if (d.value>params.work)
        return "#5cb85c";
      else
        return "#337ab7";
    else
      return "#d9534f";
  });
}
