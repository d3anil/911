function render_graph3(data, params) {
  var margin = {top: 40, right: 20, bottom: 65, left: 40},
      width = params.g_width - margin.left - margin.right,
      height = params.g_height - margin.top - margin.bottom;

  var x = d3.scale.ordinal()
      .rangeRoundBands([0, width], .1);

  var y = d3.scale.linear()
      .range([height, 0]);

  var xAxis = d3.svg.axis()
      .scale(x)
      .orient("bottom");

  var yAxis = d3.svg.axis()
      .scale(y)
      .orient("left");

  var tip = d3.tip()
    .attr('class', 'd3-tip')
    .offset([-10, 0])
    .html(function(d) {
      return "<strong>Frequency:</strong> <span style='color:red'>" + d.value + "</span>";
    })

  var svg = d3.select("#div_graph_perc").append("svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
    .append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

  svg.call(tip);

    x.domain(data.map(function(d) {
      return d.date_t;
    }))
    y.domain([0,100]);

    svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis)
      .selectAll("text");

    svg.append("g")
        .attr("class", "y axis")
        .call(yAxis)
      .append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 6)
        .attr("dy", 6)
        .style("text-anchor", "end")
        .text("Отношение, %");

    svg.selectAll(".bar")
        .data(data)
      .enter().append("rect")
        .attr("class", "bar")
        .attr("x", function(d) { return x(d.date_t); })
        .attr("width", x.rangeBand())
        .attr("y", function(d) { return y(d.value); })
        .attr("height", function(d) { return height - y(d.value); })
        .on('mouseover', tip.show)
        .on('mouseout', tip.hide);

    svg.selectAll(".bar").attr('fill', function(d, i) {
      if (d.date_t == "не работает")  return "#d9534f";
      if (d.date_t == "холостой ход")  return "#337ab7";
      if (d.date_t == "под нагрузкой")  return "#5cb85c";
      if (d.date_t == "нет данных") return "#777777";
    });

}
