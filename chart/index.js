﻿var chart = require('chart-stream')(function (url) {
  console.log('Open %s in your browser to see the chart', url)
})
 
chart.write('x*2,y/2')
plot(1, Math.pow(2, 30))
 
function plot (x, y) {
  chart.write(x + ',' + y)
  if (y <= 1) return
  setTimeout(plot, 100, x * 2, Math.round(y / 2))
}