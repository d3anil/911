var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('./database/sensor.db');

db.serialize(function() {
  db.run("CREATE TABLE number (id int AUTO_INCREMENT, value int(10) NOT NULL, date NUMERIC(21) NOT NULL, PRIMARY KEY (id));");
});

db.close();
